package se.ju.students.androidlab;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

public class MainActivity extends AppCompatActivity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }
    public void createButtonClicked(View view){
        Intent intent = new Intent(this, CreateActivity.class);
        startActivity(intent);
    }
    public void toDoButtonClicked(View view){
        Intent intent = new Intent(this, PickTodoActivity.class);
        intent.putExtra("fromDelete", false);
        startActivity(intent);
    }
    public void deleteButtonClicked(View view){
        Intent intent = new Intent(this, PickTodoActivity.class);
        intent.putExtra("fromDelete", true);
        startActivity(intent);
    }
}
