package se.ju.students.androidlab;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import static se.ju.students.androidlab.Data.todos;

public class ViewTodoActivity extends Activity {
    //public static final String EXTRA_TODO_INDEX = "todoIndex";
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_todo);
        TextView title = (TextView)findViewById(R.id.textView);
        int item = getIntent().getIntExtra("todoIndex", 0);
        boolean toDelete = getIntent().getBooleanExtra("fromDelete", false);
        if (!toDelete){
            Button deleteButton = (Button)findViewById(R.id.deleteButton);
            deleteButton.setVisibility(View.INVISIBLE);
        }
        title.setText(Data.todos.get(item).toString());


    }

    public void deleteClicked(View view){


        new AlertDialog.Builder(this)
                .setTitle("Delete ToDo")
                .setMessage("Do you really want to delete it?")
                .setPositiveButton(
                        android.R.string.yes,
                        new DialogInterface.OnClickListener(){
                            public void onClick(DialogInterface dialog, int whichButton){
                                int item = getIntent().getIntExtra("todoIndex", 0);
                                todos.remove(item);
                                finish();
                            }
                        }
                ).setNegativeButton(
                android.R.string.no,
                new DialogInterface.OnClickListener(){
                    public void onClick(DialogInterface dialog, int whichButton){
                        // Do not delete it.
                    }
                }
        ).show();

    }
}
