package se.ju.students.androidlab;

import android.app.Activity;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.View;
import android.widget.EditText;

import static se.ju.students.androidlab.Data.todos;

public class
CreateActivity extends Activity {
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create);
    }
    public void createClicked(View view){
        EditText editText = (EditText) findViewById(R.id.enteredTitle);
        String enteredTitle = editText.getText().toString();
        todos.add(new Data.ToDo(enteredTitle));
        System.out.println(todos.size());
        finish();

    }
}
